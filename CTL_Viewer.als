sig Tree{
	root: lone Node,
	successors: Node -> Node
}


abstract sig Node{

}

fact notClclic { no n : Node | 
	n in n.^(Tree.successors)
}

fact allNodeBelongToOneTree{
	all n : Node | one t : Tree | n in t.root.*(t.successors)
}

fact singleMapping{
	all n, x, y : Node |
	x -> n in Tree.successors
 	and y -> n in Tree.successors
	implies x = y
}

sig P extends Node{
}

sig Not_P extends Node{
}

fact noNode {
	all x : Node | x in P or x in Not_P 
}

pred prAU[n : Node]{

	all x : Node | x !in  n.*(Tree.successors) => x in Not_P
}



fun AG [n : Node] : set Node{ 
n.*(Tree.successors) 
}

fun AX [n : Node] : set Node{ 
n.(Tree.successors) 
}

fun AF [n : Node] : set Node{ 
{x : n.*(Tree.successors) | no x.(Tree.successors)} 
}

fun AF2 [n : Node] : set Node{
	n.^(Tree.successors)
}

fun AU [n : Node] : set Node{ 
(all x : Node | x !in  n.*(Tree.successors) => x in Not_P) => n else none }


fun test[n : Node] : set Node {  AF[AX[n]] }

//pred show() { some b: Node | test[b] in P and test[b] != none}
pred show() { some b: Node | one t : Tree | test[b] in P and test[b] != none and b = t.root}

run show for exactly 10 Node, 1 Tree 

